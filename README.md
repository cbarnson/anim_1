# Sample Animations

### Written by Cody Barnson

## **Tree**
This animation simulates the growth of a pseudo-random tree using a 
recursive method to draw branches on each line when it reaches 1/3 its
maximum length

## **Rain**
This animation simulates rain with simple drawing of allegro primitives
starting from a random initial configuration

### Compiling
Simply run <make> from command line to compile

### Dependent Libraries
Allegro 5.0 game programming library
http://liballeg.org/
